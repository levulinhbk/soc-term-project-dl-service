# Introduction
This is the flask server for my course projects, recently brought back to life.
Due to the fact that it contains some large files, I stored it on bitbucket.

# Installation
1. Install the requirements
```bash
pip install -r requirements.txt
```

2. Run the server
```bash
python py/api.py
```

# Usage
The server is running on port 2808 by default. You can change it in the `api.py` file.

Now you can start using the client to send requests to the server.
